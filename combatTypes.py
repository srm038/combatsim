from typing import *


class Weapon(TypedDict):
    type: str
    damage: str
    range: NotRequired[dict]


class Character(TypedDict):
    name: str
    initiative: int
    AP: int
    AC: int
    STR: int
    DEX: int
    THAC0: int
    morale: int
    group: str
    weapons: dict[str, Weapon]


Characters = Dict[str, Character]


class Hex(NamedTuple):
    q: int
    r: int

    def __eq__(self, other):
        return self.q == other.q and self.r == other.r


class Point(NamedTuple):
    x: float
    y: float

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y


Move = TypedDict('Move', {'from': Hex, 'to': Hex, 'stride': int})


class Attack(TypedDict):
    target: str
    hit: int
    hitBonus: int
    dmg: int
    success: bool


class Action(TypedDict):
    cost: int
    description: str


Actions = Move | Attack | Action


class Turn(TypedDict):
    character: str
    HP: int
    conscious: NotRequired[bool]
    dead: NotRequired[bool]
    location: Hex
    weapon: str
    orientation: int
    actions: list[Actions]


Round = list[Turn]

Combat = list[Round]

Block = int


class Obstacle(TypedDict):
    location: Hex
    blocked: list[Block]
    occupied: bool


Map = list[Obstacle]


class Fight(TypedDict):
    characters: Characters
    map: Map
    combat: Combat


PathVec = tuple[int, Hex]

Score = float | int

Path = tuple[Hex, list[Hex], PathVec, Score]
