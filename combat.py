import json
import math
from matplotlib import pyplot as plt
from matplotlib.patches import RegularPolygon, FancyArrow, Circle
from itertools import product, accumulate
import xdice

from combatTypes import *

strides = {1: [1], 2: [2, 3], 3: [4, 5], 4: [6, 7, 8]}
hexesAP = {1: 1, 2: 2, 3: 2, 4: 3, 5: 3, 6: 4, 7: 4, 8: 4}
vec = {
    Hex(1, 0),
    Hex(1, -1),
    Hex(0, -1),
    Hex(-1, 0),
    Hex(-1, 1),
    Hex(0, 1)
}


def loadCharacters(file: str) -> Characters:
    with open(file, 'r') as f:
        raw = json.load(f)['characters']
    characters: Characters = {}
    for name in raw:
        char: Character = cast(Character, raw[name])
        characters.update({name: char})
    return characters


def loadCombat(file: str) -> Combat:
    with open(file, 'r') as f:
        combat: Combat = json.load(f)['combat']
    for round in combat:
        for turn in round:
            if 'location' in turn:
                q, r = turn['location']
                turn['location'] = Hex(q, r)
            for a, action in enumerate(turn['actions']):
                if 'to' in action:
                    action: Move = {'from': Hex(*action['from']), 'to': Hex(*action['to']), 'stride': action['stride']}
                    turn['actions'][a] = action
    return combat


def saveCombat(fight: Fight, file: str):
    with open(file, 'w') as f:
        json.dump(fight, f)


def loadMap(file: str) -> Map:
    radius = 2 / 3
    with open(file, 'r') as f:
        battlemap: Map = json.load(f)['map']
    for obstacle in battlemap:
        q, r = obstacle['location']
        obstacle['location'] = Hex(q, r)
    for obstacle in list(battlemap):
        A = obstacle['location']
        xa, ya = hex2px(A)
        for a in obstacle.get('blocked', []):
            xb, yb = xa + radius * math.cos(math.radians(a)), ya + radius * math.sin(math.radians(a))
            B = px2hex(Point(xb, yb))
            b = (a + 180) % 360
            for o in battlemap:
                if o['location'] == B:
                    o.setdefault('blocked', [])
                    blockers: list[Block] = o.get('blocked', [])
                    if b not in blockers:
                        blockers.append(b)
                    break
            else:
                blockers: list[Block] = [b]
                reflection: Obstacle = {}
                reflection.update({'location': B, 'blocked': blockers})
                battlemap.append(reflection)
    return battlemap


def loadFight(file: str) -> Fight:
    return {'characters': loadCharacters(file), 'map': loadMap(file), 'combat': loadCombat(file)}


def drawRound(fight: Fight, n: int):
    combat = fight['combat']
    battlemap = fight['map']
    characters = fight['characters']
    fig, ax = plt.subplots(1)
    ax.set_aspect('equal')
    radius = 2 / 3

    maxQ, maxR, minQ, minR = mapExtents(battlemap, combat)

    pieces = {obstacle.get('location') for obstacle in battlemap} | {getLocation(fight, n, turn['character']) for turn
                                                                     in sum(combat, [])}
    pieces = {hex2px(piece) for piece in pieces}
    minX = min(piece.x for piece in pieces) - radius
    minY = min(piece.y for piece in pieces) - radius
    maxX = max(piece.x for piece in pieces) + radius
    maxY = max(piece.y for piece in pieces) + radius

    for q in range(minQ, maxQ + 1):
        for r in range(minR, maxR + 1):
            x, y = hex2px(Hex(q, r))
            hex = RegularPolygon((x, y), numVertices=6, radius=radius, facecolor='w', alpha=0.2, edgecolor='k',
                                 orientation=math.radians(30))
            ax.add_patch(hex)
            ax.text(x, y - 0.6 * radius, f"{q} {r}", ha='center', va='center', size=6, clip_on=True)

    for obstacle in battlemap:
        x, y = hex2px(obstacle['location'])
        for a in obstacle.get('blocked', []):
            xa, ya = x + radius * math.cos(math.radians(a - 30)), y + radius * math.sin(math.radians(a - 30))
            xb, yb = x + radius * math.cos(math.radians(a + 30)), y + radius * math.sin(math.radians(a + 30))
            ax.plot([xa, xb], [ya, yb], lw=5, c='k')
        if obstacle.get('occupied', False):
            ax.add_patch(Circle((x, y), radius=radius / 4, color='k'))

    for turn in combat[n]:
        if 'location' in turn:
            x, y = hex2px(getLocation(fight, n, turn['character']))
            angle = math.radians(turn['orientation'])
            name = turn['character']
            c = characters.get(name, {}).get('group', 'k')
            A = Point(x - 1 / 6 * math.cos(angle), y - 1 / 6 * math.sin(angle))
            B = Point(1 / 6 * math.cos(angle), 1 / 6 * math.sin(angle))
            hex = FancyArrow(A.x, A.y, B.x, B.y, color=c, width=1 / 12)
            ax.add_patch(hex)

    ax.autoscale_view()
    ax.axis("off")
    plt.axis([minX, maxX, minY, maxY])
    plt.show()
    return ax


def mapExtents(battlemap, combat) -> tuple[int, int, int, int]:
    minQloc = min(sum(combat, []), key=lambda turn: turn.get('location').q)['location'].q - 1
    minRloc = min(sum(combat, []), key=lambda turn: turn.get('location').r)['location'].r - 1
    maxQloc = max(sum(combat, []), key=lambda turn: turn.get('location').q)['location'].q + 1
    maxRloc = max(sum(combat, []), key=lambda turn: turn.get('location').r)['location'].r + 1
    minQmap = min(battlemap, key=lambda obstacle: obstacle.get('location').q)['location'].q - 1
    minRmap = min(battlemap, key=lambda obstacle: obstacle.get('location').r)['location'].r - 1
    maxQmap = max(battlemap, key=lambda obstacle: obstacle.get('location').q)['location'].q + 1
    maxRmap = max(battlemap, key=lambda obstacle: obstacle.get('location').r)['location'].r + 1
    minQ = min(minQloc, minQmap)
    minR = min(minRloc, minRmap)
    maxQ = max(maxQloc, maxQmap)
    maxR = max(maxRloc, maxRmap)
    return maxQ, maxR, minQ, minR


def hex2px(coords: Hex, size: float = 2 / 3) -> Point:
    x: float = size * (3 / 2 * coords.q)
    y: float = -size * (math.sqrt(3) / 2 * coords.q + math.sqrt(3) * coords.r)
    return Point(x, y)


def px2hex(coords: Point, size: float = 2 / 3) -> Hex:
    q: int = round((2 / 3 * coords.x) / size)
    r: int = round((-1 / 3 * coords.x - math.sqrt(3) / 3 * coords.y) / size)
    return Hex(q, r)


def remainingAP(character: Character, _round: Round) -> int:
    ap = character['AP']
    name = character['name']
    turn = next(turn for turn in _round if turn['character'] == name)
    for action in turn['actions']:
        if 'to' in action:
            ap -= 1
        elif 'hit' in action:
            ap -= 2
        elif 'cost' in action:
            ap -= action['action']['cost']
    return ap


def movementRange(fight: Fight, name: str, n: int, ap: int, attackType: str) -> list[Path]:
    if attackType not in ['ranged', 'melee']:
        raise ValueError(f"{attackType} is not an attack type")
    combat = fight['combat']
    battlemap = fight['map']
    characters = fight['characters']
    character = characters[name]
    _round = combat[n]
    turn = next(turn for turn in _round if turn['character'] == name)
    location = getLocation(fight, n, turn['character'])
    group = character['group']
    opponents = [getLocation(fight, n, turn['character']) for turn in _round if
                 characters[turn['character']]['group'] != group]

    pathsVector = list(product(sum(strides.values(), []), vec))
    paths = list(product(pathsVector, repeat=1))
    for i in range(2, ap + 1):
        comb = list(product(pathsVector, repeat=i))
        paths.extend(comb)

    potential = []
    for path in paths:
        if len(path) > 2:
            if speedChanges(path) > 1:
                continue
            if any(a not in [0, 60, 300] for a in pathAngles(path)):
                continue
        a = location
        hexPath = [a]
        for p, q in enumerate(path):
            d, v = q
            for i in range(d):
                b = hexPath[-1]
                c = Hex(hexPath[-1].q + v.q, b.r + v.r)
                if any(opp == b or opp == c for opp in opponents) or isBlocked(battlemap, b, c):
                    break
                hexPath.append(c)
            else:
                continue
            break
        else:
            potential.append((hexPath[-1], hexPath, path, 1))
    own = (location, [location], ((0, Hex(0, 0)),))
    potential.insert(0, own)

    done = set()
    for p in sorted(potential, key=lambda p: len(p[2])):
        if p[0] in done:
            potential.remove(p)
        else:
            done.add(p[0])

    for i, p in enumerate(potential):
        score = moveScore(fight, n, name, p, attackType)
        potential[i] = (p[0], p[1], p[2], score)
    return potential


def speedChanges(path: PathVec) -> int:
    changes = 0
    steps = [hexesAP[s[0]] for s in path]
    for p, q in zip(steps[:-1], steps[1:]):
        if p != q:
            changes += 1
    return changes


def pathAngles(path: PathVec) -> list[int]:
    angles = []
    hexPath = [Hex(0, 0)] + [p[1] for p in path]
    for a, b, c in zip(hexPath[:-2], hexPath[1:-1], hexPath[2:]):
        heading = hexAngle(a, Hex(a.q + b.q, a.r + b.r))
        theta = hexAngle(b, Hex(c.q + b.q, c.r + b.r))
        angles.append((theta - heading) % 360)
    return angles


def isBlocked(battlemap: Map, A: Hex, B: Hex) -> bool:
    blocked: list[bool] = []
    for obstacle in battlemap:
        for a in obstacle.get('blocked', []):
            if obstacle['location'] == A and a == hexAngle(A, B):
                blocked.append(True)
            elif obstacle['location'] == B and a == hexAngle(B, A):
                blocked.append(True)
    return any(blocked)


def showMovementRange(ax, fight: Fight, n: int, name: str, potential: list[Path]):
    combat = fight['combat']
    character = fight['characters'][name]
    _round = combat[n]
    minScore = min(p[3] for p in potential)
    maxScore = max(p[3] for p in potential)

    def scoreAlpha(s: Score) -> float:
        return rescale(s, [minScore, maxScore], [0.05, 0.75])

    done = set()
    for p in potential:
        if p[0] in done:
            continue
        done.add(p[0])
        x, y = hex2px(p[0])
        score = scoreAlpha(p[3])
        hex = RegularPolygon((x, y), numVertices=6, radius=2 / 3, alpha=score, edgecolor=None,
                             fc=character['group'], orientation=math.radians(30))
        ax.add_patch(hex)
    ax.autoscale_view()


def neighbors(hex: Hex) -> set[Hex]:
    return {
        Hex(hex.q + 1, hex.r),
        Hex(hex.q + 1, hex.r - 1),
        Hex(hex.q, hex.r - 1),
        Hex(hex.q - 1, hex.r),
        Hex(hex.q - 1, hex.r + 1),
        Hex(hex.q, hex.r + 1)
    }


def travelStraight(A: Hex, v: Hex) -> Hex:
    return Hex(A.q + v.q, A.r + v.r)


def hexAngle(A: Hex, B: Hex) -> int:
    xa, ya = hex2px(A)
    xb, yb = hex2px(B)
    return round(math.degrees(math.atan2(yb - ya, xb - xa))) % 360


def pxDistance(A: Hex, B: Hex) -> float:
    xa, ya = hex2px(A)
    xb, yb = hex2px(B)
    return round(math.hypot(yb - ya, xb - xa), 1)


def hexDistance(A: Hex, B: Hex) -> int:
    return round((abs(A.q - B.q) + abs(A.q + A.r - B.q - B.r) + abs(A.r - B.r)) / 2)


def moveScore(fight: Fight, n: int, name: str, path: Path, attackType: str) -> Score:
    if attackType not in ['ranged', 'melee']:
        raise ValueError(f"{attackType} is not an attack type")
    group = fight['characters'][name]['group']
    opponents = [getLocation(fight, n, turn['character']) for turn in fight['combat'][n] if
                 fight['characters'][turn['character']]['group'] != group]
    allies = [getLocation(fight, n, turn['character']) for turn in fight['combat'][n] if
              fight['characters'][turn['character']]['group'] == group and turn['character'] != name]

    end = path[0]
    score = -len(path[2])
    score -= int(end in allies)
    if attackType == 'melee':
        melee = meleeCount(fight, n, name, end)
        if not melee:
            score -= 1
        elif melee == 1:
            score += 1
        else:
            score -= melee
        score += melee if melee == 1 and len(path[1]) == 1 else 0

        for a, b in zip(path[1][:-1], path[1][1:]):
            if leaveMelee(fight, n, name, a, b):
                score -= 1

        for b in neighbors(end):
            if b in opponents:
                opp = next(c for c in fight['combat'][n] if getLocation(fight, n, c['character']) == b)
                if isBlocked(fight['map'], b, getLocation(fight, n, opp['character'])):
                    continue
                score += threat(fight, n, name, opp['character'])
                score += isFlank(fight, n, getLocation(fight, n, opp['character']), end)
    if attackType == 'ranged':
        melee = meleeCount(fight, n, name, end)
        ranged = rangeCount(fight, n, name, end)
        score += len(opponents) - ranged if ranged else -len(opponents)
        score -= melee

    return score


def meleeCount(fight: Fight, n: int, name: str, b: Hex) -> int:
    opponents = [opp['location'] for opp in getOpponents(fight, n, name)]
    return sum(h in opponents for h in neighbors(b) if not isBlocked(fight['map'], h, b))


def rangeCount(fight: Fight, n: int, name: str, h: Hex) -> int:
    group = fight['characters'][name]['group']
    _round = fight['combat'][n]
    opponents = [turn for turn in _round if fight['characters'][turn['character']]['group'] != group]
    allies = [getLocation(fight, n, turn['character']) for turn in _round if
              fight['characters'][turn['character']]['group'] == group]
    obscured = {obstacle.get('location') for obstacle in fight['map'] if obstacle.get('occupied')}
    ranged = 0
    for opp in opponents:
        B = getLocation(fight, n, opp['character'])
        line = lineBetween(h, B)
        for a, b, c, d in zip(line[:-1], line[1:], line[::-1][:-1], line[::-1][1:]):
            if isBlocked(fight['map'], a, b) or a in obscured or b in allies:
                break
            if isBlocked(fight['map'], d, c) or d in obscured or c in allies:
                break
        else:
            ranged += 1
    return ranged


def leaveMelee(fight: Fight, n: int, nameA: str, A: Hex, B: Hex) -> bool:
    if A == B:
        return False
    characters = fight['characters']
    group = characters[nameA]['group']
    opponents = [getLocation(fight, n, turn['character']) for turn in fight['combat'][n] if
                 characters[turn['character']]['group'] != group]
    for opp in opponents:
        if A in neighbors(opp) and B not in neighbors(opp):
            return True
    return False


def isFlank(fight: Fight, n: int, opp: Hex, b: Hex) -> int:
    if isBlocked(fight['map'], opp, b):
        return 0
    theta = next(c for c in fight['combat'][n] if getLocation(fight, n, c['character']) == opp)['orientation']
    alpha = hexAngle(opp, b)
    beta = (theta - alpha) % 360
    if beta in [60, 0, 300]:
        return 0
    if beta in [120, 240]:
        return 1
    return 2


def pathStride(path: Path, A: Hex) -> int:
    hexPath = path[1]
    if A not in hexPath:
        return False
    steps = path[2]
    idx = hexPath[1:].index(A)
    hexStride = 0
    for a, s in zip(accumulate([hexesAP[s[0]] for s in steps]), [hexesAP[s[0]] for s in steps]):
        hexStride = s
        if idx < a:
            break
    return hexStride


def rescale(x: float | int, A: list[int | float], B: list[int | float]) -> int | float:
    return (((x - A[0]) * (B[1] - B[0])) / (A[1] - A[0])) + B[0]


def threat(fight: Fight, n: int, name: str, opp: str) -> Score:
    try:
        char = next(c for c in fight['combat'][n] if c['character'] == name)
        opp = next(c for c in fight['combat'][n] if c['character'] == opp)
    except StopIteration:
        return 0
    return 3 / hexDistance(getLocation(fight, n, name), getLocation(fight, n, opp['character']))


def lineBetween(A: Hex, B: Hex, size: float = 2 / 3) -> list[Hex]:
    d = hexDistance(A, B)
    if d == 0:
        return [A]
    a = hex2px(A)
    b = hex2px(B)
    path: list[Hex] = []
    for i in range(d + 1):
        c = Point(a.x + (b.x - a.x) * i / d, a.y + (b.y - a.y) * i / d)
        path.append(px2hex(c))
    return path


def fov(fight: Fight, n: int, name: str) -> set[Hex]:
    field: set[Hex] = set()
    char = next(c for c in fight['combat'][n] if c['character'] == name)
    A = getLocation(fight, n, name)
    maxQ, maxR, minQ, minR = mapExtents(fight['map'], fight['combat'])
    obscured = {obstacle.get('location') for obstacle in fight['map'] if obstacle.get('occupied')}
    for q in range(minQ, maxQ + 1):
        for r in range(minR, maxR + 1):
            B = Hex(q, r)
            line = lineBetween(A, B)
            for a, b in zip(line[:-1], line[1:]):
                if isBlocked(fight['map'], a, b) or a in obscured:
                    break
            else:
                field.add(B)
    return field


def showFov(ax, fight: Fight, n: int, name: str):
    combat = fight['combat']
    character = fight['characters'][name]
    _round = combat[n]
    field = fov(fight, n, name)

    for p in field:
        x, y = hex2px(p)
        hex = RegularPolygon((x, y), numVertices=6, radius=2 / 3, alpha=0.1, edgecolor=None,
                             fc=character['group'], orientation=math.radians(30))
        ax.add_patch(hex)
    ax.autoscale_view()


def getLocation(fight: Fight, n: int, name: str) -> Hex:
    char = next(c for c in fight['combat'][n][::-1] if c['character'] == name)
    loc = char['location']
    for action in char['actions'][::-1]:
        if 'to' in action:
            loc = action["to"]
            break
    return loc


def addMove(fight: Fight, n: int, name: str, path: Path):
    char = next(c for c in fight['combat'][n][::-1] if c['character'] == name)
    loc = getLocation(fight, n, name)
    for p in path[2]:
        d, v = p
        b = Hex(loc.q + d * v.q, loc.r + d * v.r)
        stride = hexesAP[d]
        char['actions'].append(move(fight, n, name, b, stride))


def move(fight: Fight, n: int, name: str, b: Hex, stride: int) -> Move:
    loc = getLocation(fight, n, name)
    return {'from': loc, 'to': b, 'stride': stride}


def getOpponents(fight: Fight, n: int, name: str) -> list[Turn]:
    group = fight['characters'][name]['group']
    opponents = [turn for turn in fight['combat'][n] if
                 fight['characters'][turn['character']]['group'] != group and not turn.get('dead')]
    return opponents


def getMeleeTarget(fight: Fight, n: int, name: str) -> Turn:
    loc = getLocation(fight, n, name)
    opponents = filter(
        lambda opp: getLocation(fight, n, opp['characater']) in neighbors(loc) and
                    not isBlocked(fight['map'], loc, getLocation(fight, n, opp['characater'])),
        getOpponents(fight, n, name))
    return max(opponents, key=lambda opp: threat(fight, n, name, opp['character']))


def getRangedTarget(fight: Fight, n: int, name: str) -> Turn:
    loc = getLocation(fight, n, name)
    field = fov(fight, n, name)
    opponents = filter(
        lambda opp: not isBlocked(fight['map'], loc, getLocation(fight, n, opp['character'])) and
                    getLocation(fight, n, opp['character']) in field, getOpponents(fight, n, name))
    return max(opponents, key=lambda opp: threat(fight, n, name, opp['character']))


def attack(fight: Fight, n: int, name: str, target: Turn, attackType: str) -> Attack:
    if attackType not in ['ranged', 'melee']:
        raise ValueError(f"{attackType} is not an attack type")
    char = next(c for c in fight['combat'][n][::-1] if c['character'] == name)
    STR = fight['characters'][name]['STR']
    dist = hexDistance(getLocation(fight, 0, name), getLocation(fight, 0, target['character']))
    hitBonus = {'melee': getMeleeBonus(fight, name), 'ranged': getRangedBonus(fight, n, name, dist)}[attackType]
    dmgBonus = {3: -2, 4: -1, 17: 1, 18: 2, 19: 7, 20: 8, 21: 9, 22: 10, 23: 11, 24: 12, 25: 14}.get(STR, 0)
    dmgBonus *= 0 if attackType == 'melee' else 1
    hit = xdice.roll('1d20')
    dmg = xdice.roll(fight['characters'][name]['weapons'][char['weapon']]['damage']) + dmgBonus
    THAC0 = fight['characters'][name].get('THAC0', 20)
    AC = fight['characters'][target['character']].get('AC', 10)
    success = (hit + hitBonus) >= (THAC0 - AC)
    return {'target': target['character'], 'hit': hit, 'hitBonus': hitBonus, 'dmg': dmg, 'success': success}


def getMeleeBonus(fight: Fight, name: str) -> int:
    STR = fight['characters'][name]['STR']
    hitBonus = {3: -3, 4: -2, 5: -1, 17: 1, 18: 1, 19: 3, 20: 3, 21: 4, 22: 4, 23: 5, 24: 5, 25: 7}.get(STR, 0)
    return hitBonus


def getRangedBonus(fight: Fight, n: int, name: str, dist: int) -> int:
    char = next(c for c in fight['combat'][n][::-1] if c['character'] == name)
    DEX = fight['characters'][name]['DEX']
    _range = fight['characters'][name]['weapons'][char['weapon']]['range']
    for r in sorted(_range, key=lambda r: _range[r]):
        if _range[r] >= dist:
            break
    hitBonus = {'point blank': 1, 'medium': -2, 'long': -5}.get(r, 0)
    hitBonus += {3: -3, 4: -2, 5: -1, 16: 1, 17: 2, 18: 2}.get(DEX, 0)
    return hitBonus


def makeAttack(fight: Fight, n: int, name: str, attackType: str):
    if attackType not in ['ranged', 'melee']:
        raise ValueError(f"{attackType} is not an attack type")
    target = {'melee': getMeleeTarget, 'ranged': getRangedTarget}[attackType](fight, n, name)
    char = next(c for c in fight['combat'][n][::-1] if c['character'] == name)
    attackResult = attack(fight, n, name, target, attackType)
    char['actions'].append(attackResult)


def getHP(fight: Fight, n: int, name: str) -> int:
    char = next(c for c in fight['combat'][n][::-1] if c['character'] == name)
    hp = char['HP']
    for _round in fight['combat']:
        for c in _round:
            if c == char:
                continue
            for action in c['actions']:
                if action.get('target') == name and action.get('success'):
                    hp -= action.get('dmg', 0)
    return hp
